const {it, describe} = require('mocha');
const {assert, should} = require('chai');

describe('Realizar un health-check', () => {


    it('Obtener health-check', () => {

        const {getHealthCheck, productsRoute} = require('../app/routes/api/v1/products.route');
        const response = getHealthCheck();

        assert.isObject(response);
        assert.isBoolean(response.sucess);
        assert.isString(response.message);
    });

})
