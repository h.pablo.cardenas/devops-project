const route = require('express').Router();

const apiVersion = '/api/v1';

route.get(apiVersion+'/health-check', (req, res, next) => {
    const response = getHealthCheck(res);

    return res.send(response);
})

function getHealthCheck(res) {
    return {sucess: true, message: 'Hello'};
}

module.exports.productsRoute = route;
module.exports.getHealthCheck = getHealthCheck;
