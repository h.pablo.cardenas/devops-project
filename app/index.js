require('dotenv').config();
const express = require('express');
const {productsRoute} = require('./routes/api/v1/products.route');

const app = express();

app.use(express.urlencoded({extended: true}));
app.use(express.json());

app.use(productsRoute);


const port = process.env.APP_PORT;
const host = process.env.APP_HOST;

app.listen(port, host, () => {
    console.log('App is up and listening on http://'+host+':'+port);
});
